---
theme: gaia
_class: lead
paginate: true
backgroundColor: #ffff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
footer: February 2021
marp: true
---


<style>
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
</style>


![left:30% 70%](assets/Duke.svg)

# **JVM Architecture**

###### JVM in Details: part 2 of N

Pedro Jordão 

---

# Presenter

Pedro Jordão (He/Him)

* Software Developer for 6 years
* C++, Rust and Fortran (:weary:) for fast simulation code
* JVM based languages for backend and GUI
* Currently working with the Foresight team @criticaltechworks using Scala
* Rust CoP & Functional Programming CoP

---

# This Series

* Part 0: Past, Present and Future
* Part 1: JVM Architecture
* Part 2: JVM, JIT, AOT and YOU (Today!)
* Part 3: GraalVM 🏆
* Part 4: Java Memory Model (Or How To Correctly Implement Singletons)
* Part 5: Garbage Collectors? (accepting suggestions)

---

# A Letter Soup

* JVM: **J**ava **V**irtual **M**achnine
* JIT: **J**ust **I**n **T**ime (compiler)
* AOT: **A**head **O**f **T**ime (compiler)

---

# Compilers vs Interpreters

* Roughly speaking, compilers transform code from a high level language into code of a lower level language 
  * The lower level language might be actual *plataform code*
    * e.g.: C compiles down to assembly and is then assembled into a binary
  * Compilation might take some time, but the execution is faster
* Interpreters execute code as they parse it 
  * No compilation time necessary
  * Slower execution

---

# Compilers vs Interpreters: Virtual Machines

* Languages such as Java and C# take the best of both worlds
  * Both compile down to *platform code* through an *Intermediate Language* 
  * The *platform* where Java runs is the *JVM*, which has *Java bytecode* as its language

---

# Why We Are Talking About This Now

* Well, because I want to
* Also, a lot of cool developments happening on GraalVM
* Recently OpenJDK kicked off Project Layden, which will focus on AOT compilation
  - [Project Layden Announcement](https://mail.openjdk.java.net/pipermail/discuss/2020-April/005429.html)

---

# JVM Architecture

![center](assets/jvm.png)

---

# What We Will be Looking at Today

* The optimizations implemented by OpenJDK JIT (a.k.a. Hotspot)
* OpenJDK JIT tiered implementation 
* AOT compilation introduced in Java 9
* How it compares to traditional compiled languages
  - I hope you like assembly!
* Industry directions

---

# What We Will NOT be Looking at Today

* In depth GraalVM 
* How to read assembly
* How to actually implement a JIT
* _How to make your code 10x faster with this one easy trick_

---

# Current JVM Implementations

* There are way too many to list here, but the main (active) ones:
  - HotSpot (Oracle, OpenJDK)
  - GraalVM (Oracle)
  - Corretto (Amazon)
  - OpenJ9 (IBM)
  - Zulu (Azul)
* HotSpot under OpenJDK is the defacto standard and we will focus on it

---

# History of JIT in the HotSpot VM

- Java 1.0, Released in 1996 did not include any kind of JIT
- Java 1.2, Released in 1998 included a JIT compiler for the first time
- Java 7, Released in 2011 included the first version of tiered compilation, disabled by default
- Java 8, Released in 2014 enabled tiered compilation by default 

---

# What Is JIT Anyway?


![center](assets/JIT.jpg)

* Runs WITH the application and compiles the Java Bytecode into native assembly
* Able to observe the application behavior and optimize accordingly 

---

# What Is JIT Anyway?


![center](assets/JIT.jpg)

* Able to make assumptions regarding the execution of the application
* Has overhead (both CPU and Memory)
* Before JITing the code is still interpreted (i.e. slower)

---

# A Look At JIT 

* The current implementation of JIT on HotSpot is a _tiered_ JIT
* Two compilers, C1 ("client") and C2 ("server")
  - C1 compilation takes less time, but the code generation is not fully optimized
  - C2 compilation take more time, but generated code is faster

---

# A Look At JIT 

![center](assets/tiers.jpg)

* Tiered compilation contains 4 "tiers" 
  - The end goal is to get to tier 1 or 4 
  - On HotSpot hot functions and hot loops are compiled, but also warm methods with warm loops


---

# A Look At JIT 

* Tier 1: No instrumentation
  - No overhead
* Tier 2: Counters (how many times the function/loop was executed) 
  - Negligible overhead
* Tier 3: Profiling (branch prediction)
  - Some overhead (30%-ish)

  
---

# Demo

* Performance characteristics
  - (`src/its-a-trap`)

<!--
The code runs two loops. Most of the time the inner loop does nothing since trap != null
Around the external execution 370 the code gets optimized and the execution time is heavily 
reduced. This is because the code is optimized for the case trap != null

At 400 the execution goes to up again since we hit a deoptimization trap when our assumption
gets proven wrong
-->

---

# Demo

* Tiered Compilation
  - (`src/tiered-compilation`)

<!-- 
Same thing as the previous, but without the trap

As before, at some point (around 180) the code gets optimized

Now lets see the inner workings of the JVM 

First column: Timestamp
Second column: Compilation ID
    This one is tricky, there are 2 compilation IDs going on at
    the same time. 
    Compilation with the % attribute are OSR (On Stack Replacement)
    compilations, we will talk more about these later.
    So compilation IDs 5 and 5% are different compilations.
Third column: Method Attributes
    %: OSR 
    s: Synchronized
    !: Method has exception handler
    b: Blocking Compilation
    n: Native Wrapper
Forth Column: Tiered Compilation Level
Fifth Column: Method Name
Sixth Column: Method Size and Stack Replacement (if applicable)
Seventh Column: Deoptimization
    made not entrant: Deoptimization happened 
    made zombie: no activateions after deoptimization happen 
                 (we will not see this one today)
-->

---

# A Look At JIT 

* We will focus mostly on C2 from now on.
* During application startup profiling is not applied
  - The reason is simple: Applications have many one-time-only code paths that will never happen again. No point optimizing for it, adding start up overhead when it won't be used again.

---

# A Look At JIT 

* Optimizations
  - Profiled methods "trace" common code paths
  - If a branch "always" happen the compiler optimizes for it and ignores any other branch.
  - If this assumption is proven wrong the code is deoptimized and goes back to being interpreted.

---

# Demo

* Optimizations 
  - (`src/optimizations`)

 <!-- 
 Every reference that gets dereferenced gets checked for null
 Every array access gets checked for bounds

 Ctrl+C when it gets stuck

 C1 generated code is quite long

For C2 we can see the OptoAssembly which is an assembly-like language
that describes the data flow traced during profiling

The Assembly starts around line 2092

rsi refers to this
rds the parameter (consumer)

We begging by adjusting the class pointer to remove the HEAD block

r10d stores elementData, then the array length
notice that in case of exception we get dispatched to an exception handler

test sets an internal zero flag (ZF) when the AND operation is zero, 
for test r10d r10d it will only happen if both are zero, i.e. r10d is zero

jbe jumps if CF == 1 || ZF == 1, but test sets CF to zero, so our check is 
only if ZF == 1m which only happens if r10d is zero

This causes a jump to what will eventually return 

Second is the same, but is put there by the array access (i.e. we don't want
to try to access an array of size 0), and if the test passes (i.e. r10d contains
zero) we go to an exception handler.

Then we enter in a loop by decrementing the r10d register.

 -->

---

# A Look At JIT 

* Pros: 
  - Easy to use on the JVM
  - Optimizes for the common case
  - Can use profiling data for further optimization (i.e. is able to adapt at runtime)

---

# A Look At JIT 

* Cons: 
  - Slow start up
  - Takes some time to achieve a steady state
  - Takes resources away from your application (CPU & Memory)

---

# What Is AOT Anyway?

* Java release 9 introduced de `jaotc` command line utility.
* Pre compiles a `.class` to a platform shared object (e.g. `.so` for Linux)
* The user must then link the shared object to the JVM during start up
* You can even pre-compile the whole `java.base` module!
* This is *NOT* the same thing as GraalVM's Native Images  
  - No full application executable is generated

---

# What iS AOT Anyway?

* Some safety nets need to exist
  - If at load time the class fingerprint of the Native Library does not match the `.class` being loaded it will not run.
  - AOT is not able to make inferences about class hierarchy 
    - Most inlining is not applied

---

# Inlining Issues Example


![center](assets/inline.png)

---

# Interactions with JIT

* You can run the compiled code with JIT enabled or disabled
  - By default the JIT will be disabled for AOT compiled classes
* Enabling JIT allows faster start up with eventual faster execution at the cost of still having higher CPU and Memory usage
* Keeping it disabled reduces start up time, CPU and Memory usage, but with lower peek performance (usually) 

---

# What Is AOT Anyway?

* Pros:
  - Faster start up time
  - Lower Memory and CPU usage (no need to compile at runtime)
  - Can still use JIT (invalidating the Pro above)

---

# What Is AOT Anyway?

* Cons:
  - No profiling guided optimizations 
  - More complex to setup 
    - You need to understand what parts of your application gain by being compiled
  - *Not platform neutral* 

---

# Demo

* AOT usage
  - (`src/aot`)

<!-- 
Change: start i at 1
It will still load from aot, but when it is used it will reload the class

-->

---

# AOT VS Native Images

* We will not go into details about Native Images today
* But a quick comparison:
  - Native Image does not handle dynamic loading well (or at all)
  - Native Images essentially executes the code during compilation to get profiling information
  - Native Images are much easier to set up

---

# JIT vs C++ Demo

* This will be a completely meaningless comparison, but let's have some fun.
  - (`src/cpp-vs-jit`)

--- 

# Industry Directions

* Cached JIT
  - Another option is to cache JITed code between runs
  - Has performance benefits after the first run
  - Code can still be reJITed
  - Implemented on OpenJ9 and Azul Falcon JIT

--- 

# Industry Directions

* JITaaS - JIT as a Service
  - Yes, serious - But highly experimental
  - If the same code is being executed in multiple JVMs why should each executor spend CPU and Memory doing the same thing independently (JITing)?
  - Example: multiple PODs on Kubernetes
--- 

# Industry Directions

* JITaaS - JIT as a Service
  - JITaaS sets up a compiler server that collects profiling data, compiles in a centralized manner and distributes the compiled code
  - Implemented on OpenJ9
    - More Info: [https://blog.openj9.org/2020/01/09/free-your-jvm-from-the-jit-with-jitserver-technology/](https://blog.openj9.org/2020/01/09/free-your-jvm-from-the-jit-with-jitserver-technology/)

---

# Next Up

* Native Images And GraalVM
  - Actually generating executables with Native Images
  - Using PolyglotVM
  - Running Rust and C++ on the JVM (sort of)

---

# That's It

* Questions?