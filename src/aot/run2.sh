#!/bin/bash -e

JAVA_PATH=/opt/jdk14/bin 

clear

echo "
Now a quick comparison with our ItsATrap example. 

First, let's recap the timings by re-running the example just using JIT
"

read -p "Press enter to continue"
clear

$JAVA_PATH/javac ../its-a-trap/ItsATrap.java && $JAVA_PATH/java -cp ../its-a-trap ItsATrap | nvim -R

read -p "Press enter to continue"
clear

echo "
If we compile it natively and execute with 

jaotc --output ../its-a-trap/ItsATrap.so --directory ./its-a-trap
java -XX:+UnlockExperimentalVMOptions -XX:AOTLibrary=../its-a-trap/ItsATrap.so -cp ../its-a-trap ItsATrap 
"

read -p "Press enter to continue"
clear

$JAVA_PATH/jaotc --output ../its-a-trap/ItsATrap.so --directory ../its-a-trap
$JAVA_PATH/java -XX:+UnlockExperimentalVMOptions -XX:AOTLibrary=../its-a-trap/ItsATrap.so -cp ../its-a-trap ItsATrap | nvim -R


echo "
We can try to improve peek performance by reenabling JIT by compiling with

jaotc --compile-for-tiered --output ../its-a-trap/ItsATrap.so --directory ./its-a-trap
"

read -p "Press enter to continue"
clear


echo "
Notice that even though ItsATrap is a self contained application it still is
compiled as a native library and not as an executable!

It still needs the JVM infrastructure to run.

"

read -p "Press enter to continue"
clear

echo "
And what about the generated assembly?

It can be pretty easily retrieved and read with

objdump -d ../its-a-trap/ItsATrap.so
"

read -p "Press enter to continue"
clear

objdump -M intel-d ../its-a-trap/ItsATrap.so | nvim -R
