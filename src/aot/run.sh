#!/bin/bash -e

JAVA_PATH=/opt/jdk14/bin 

clear

echo "
Once again we will be using JDK14 for this example

Given the following class

"

bat MyArrayList.java

echo "

We first need to compile it into bytecode with 

javac MyArrayList.java

And then compile it natively using jaotc

jaotc --output MyArrayList.so MyArrayList.class
"

$JAVA_PATH/javac MyArrayList.java
$JAVA_PATH/jaotc --output MyArrayList.so MyArrayList.class

read -p "Press enter to continue"
clear

echo "
Next we will use the class with the following code


"

bat Main.java

echo "

And compile it with 

javac -cp ./ Main.java

Notice that we still need to add the bytecode compiled dependency 
to the classpath during compilation
"

$JAVA_PATH/javac -cp ./ Main.java

read -p "Press enter to continue"
clear

echo "

Finally, we can execute Main using the natively AOT compiled dependency with

java -XX:AOTLibrary=./MyArrayList.so Main

We will add a few more options so we can get more runtime information:

java -XX:+UnlockExperimentalVMOptions -XX:+PrintAOT -XX:AOTLibrary=./MyArrayList.so -verbose Main
"

read -p "Press enter to continue"
clear


$JAVA_PATH/java -XX:+UnlockExperimentalVMOptions -XX:+PrintAOT -XX:AOTLibrary=./MyArrayList.so -verbose Main > output.txt
nvim output.txt

read -p "Press enter to continue"
clear

echo "
But if we apply any modifications to MyArrayList and only compile it to bytecode, but not native it is not used
"

read -p "Press enter to continue"

nvim MyArrayList.java && $JAVA_PATH/javac MyArrayList.java
$JAVA_PATH/java -XX:+UnlockExperimentalVMOptions -XX:+PrintAOT -XX:AOTLibrary=./MyArrayList.so -verbose Main > output.txt 
nvim output.txt

