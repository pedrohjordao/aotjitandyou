import java.util.function.Consumer;
import java.util.Arrays;

public class MyArrayList<E> {
    private final E[] elementData;

    public MyArrayList(E[] elements) {
        this.elementData = elements;
    }        

    public void forEach(Consumer<? super E> action) {
        for (int i = 1; i < this.elementData.length; i++) {
            action.accept(this.elementData[i]);
        }
    }

}
