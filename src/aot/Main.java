import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        var array = new Object[10];
        Arrays.fill(array, new Object());
        var local = new MyArrayList<>(array);

        for (int i = 0; i < 1_000_000; i++) {
            local.forEach(x -> {
                var t = new Object() == x;
            });
        }
    }
}
