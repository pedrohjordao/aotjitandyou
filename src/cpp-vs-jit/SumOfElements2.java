import java.util.Random;
import java.util.Arrays;

public class SumOfElements2 {
    
  public static void main(String[] args) {
    long r = 0L;
    for (int i = 1; i < 1000; i++) {
      var length = i;
      var array = new int[length];
      var rand = new Random();
      Arrays.setAll(array, p -> rand.nextInt());
      r += doit(array, length);
    }
  }
  
  static int doit(int[] array, int length) {
    int sm = 0;
    for (
      int i = 0; 
      i < length; 
      i++
    ) sm += array[i];
    return sm;
  }
}
