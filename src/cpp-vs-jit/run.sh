#!/bin/bash -e

JAVA_PATH=../../jdk14/build/linux-x86_64-server-fastdebug/images/jdk/bin

clear

echo "
THIS IS JUST FOR FUN

Do not take this comparison too seariously. 

That being said, we will be comparing this piece of java code

"

bat SumOfElements.java

echo "
With the equivalent C++ code
"

bat sum-of-elements.cpp

echo ""
read -p "Press enter to continue"
clear

echo "
Our focus will be the code generated for the doit function. In the Java application we had to
add a loop on main to force the execution so the code can actually be JITed.

We will compile the C++ code using GCC version 9.3.0 with the -O3 -fPIC and -shared flags.
The will also extract the C2 JITed assembly from the JDK14 we have compiled with debugging enabled.
"

gcc -O3 -shared -o sum-of-elements.so sum-of-elements.cpp
$JAVA_PATH/javac SumOfElements.java
$JAVA_PATH/java -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly -XX:CompileThreshold=1 -XX:CompileOnly=SumOfElements::doit -XX:PrintAssemblyOptions=intel SumOfElements >> SumOfElementsJava.asm

read -p "Press enter to continue"
clear

echo "
How does the compiled C++ code look?
"
read -p "Press enter to continue"
clear

objdump -M intel -d sum-of-elements.so | nvim -R

read -p "Press enter to continue"
clear

echo "
Well, that seems super optimized, what about Java?
"

read -p "Press enter to continue"
clear

nvim -R SumOfElementsJava.asm

read -p "Press enter to continue"
clear

echo "
Yes, C++ is magical.

Let's tweak it a little:
"

bat sum-of-elements2.cpp


read -p "Press enter to continue"
clear

echo "
And the equivalent Java
"

bat SumOfElements2.java

read -p "Press enter to continue"
clear

gcc -O3 -shared -o sum-of-elements2.so sum-of-elements2.cpp
$JAVA_PATH/javac SumOfElements2.java
$JAVA_PATH/java -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly -XX:CompileThreshold=1 -XX:CompileOnly=SumOfElements2::doit -XX:PrintAssemblyOptions=intel SumOfElements2 >> SumOfElementsJava2.asm

read -p "Press enter to continue"
clear

echo "Again, the C++ compiled code"
read -p "Press enter to continue"
objdump -M intel -d sum-of-elements2.so | nvim -R
clear
echo "And Java"
read -p "Press enter to continue"
nvim -R SumOfElementsJava2.asm
clear