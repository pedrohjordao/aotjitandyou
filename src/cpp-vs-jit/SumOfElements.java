import java.util.Random;

public class SumOfElements {
    
  public static void main(String[] args) {
    long r = 0L;
    for (int i = 0; i < 100000000; i++) {
      r += doit();
    }
  }
  
  static int doit() {
    int sm = 0;
    for (
      int i = 0; 
      i < 100; 
      i++
    ) sm += i;
    return sm;
  }
}
