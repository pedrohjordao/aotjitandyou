int doit(int* array, int length) {
  int sm = 0;
  for (int i = 0; i < length; i++) sm += array[i];
  return sm;
}