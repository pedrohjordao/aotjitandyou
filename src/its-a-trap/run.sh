#!/bin/bash -e

JAVA_PATH=/opt/jdk14/bin

clear

$JAVA_PATH/java -version

read -p "Press enter to continue"
clear

bat ItsATrap.java

read -p "Press enter to continue"

$JAVA_PATH/javac ItsATrap.java && $JAVA_PATH/java ItsATrap | nvim -R
