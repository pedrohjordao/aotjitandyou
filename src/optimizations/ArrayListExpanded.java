import java.util.function.Consumer;

public class ArrayList<E> {
    private final E[] elementData;

    public ArrayList(E[] elements) {
        this.elementData = elements;
    }        

    public void forEach(Consumer<? super E> action) {
        if (this == null) throw new NullPointerException();
        if (this.elementData == null) throw new NullPointerException();

        for (int i = 0; i < this.elementData.length; ++i) {
            if (this == null) throw new NullPointerException();
            if (this.elementData == null) throw new NullPointerException();
            if (i < 0) throw new ArrayIndexOutOfBoundsException();
            if (i >= this.elementData.length) throw new ArrayIndexOutOfBoundsException();

            if (action == null) throw new IllegalArgumentException();
            action.accept(this.elementData[i]);
        }
    }
}
