import java.util.function.Consumer;

public class ArrayList<E> {
    private final E[] elementData;

    public ArrayList(E[] elements) {
        this.elementData = elements;
    }        

    public void forEach(Consumer<? super E> action) {
        if (this.elementData == null) throw new NullPointerException();

        for (int i = 0; i < this.elementData.length; ++i) {
            if (this.elementData == null) throw new NullPointerException();

            if (action == null) throw new IllegalArgumentException();
            action.accept(this.elementData[i]);
        }
    }
}
