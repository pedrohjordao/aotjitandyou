import java.util.function.Consumer;
import java.util.Arrays;

public class ArrayList<E> {
    private final E[] elementData;

    public ArrayList(E[] elements) {
        this.elementData = elements;
    }        

    public void forEach(Consumer<? super E> action) {
        for (int i = 0; i < this.elementData.length; ++i) {
            action.accept(this.elementData[i]);
        }
    }

    public static void main(String[] args) {
        var array = new Object[10];
        Arrays.fill(array, new Object());
        var local = new ArrayList<>(array);

        for (int i = 0; i < 1_000_000_000; i++) {
            local.forEach(x -> {
                var t = new Object() == x;
            });
        }
    }
}
