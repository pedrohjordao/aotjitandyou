#!/bin/bash -e

clear

bat ArrayList.java

echo "
Let's unpack how the JIT compiler thinks.

We will focus on the forEach method.
Implictly that piece of code expands to something like this.
"

read -p "Press enter to continue"
clear

bat ArrayListExpanded.java

read -p "Press enter to continue"
clear

echo "
We can easily eliminate the check this == null since it is always true 
per the specification.

We also know that 0 <= elementData.length <= INT_MAX. The increment 
in the for is an addition, by one, limited to elementData.length.

The compiler is capable of removing the checks on i with this information.

Right now we can't remove any other checks, so the code would look something
like the following 


"

read -p "Press enter to continue"
bat ArrayListExpandedSimplified.java

read -p "Press enter to continue"
clear

echo "
Ever wandered how the JITed code looks like?

It is a little tricky to get this information out of the HotSpot.

First you need to checkout the JDK source code. 

I'll show the steps using Mercurial, which is the versioning system used by OpenJDK,
but since I wrote this they started migrating to Git. 

Finally, I'll be basing it on JDK14. Doing this for JDK 8 on a modern system is a pain,
I don't recommend trying.

First, clone the JDK Repo

hg clone http://hg.openjdk.java.net/jdk/jdk14

This will take a long, long, LONG time.

You will need a bootstrap JDK locally, here we are using JDK 14 at /opt/jdk14

You might get some errors in the following step, but it should guide you through 
missing dependencies.

chmod +x configure
./configure --enable-debug --with-boot-jdk=/opt/jdk14 --disable-warnings-as-errors

Finally we can compile the JDK

make images

The compiled binaries will be at ./jdk14/build/linux-x86_64-server-fastdebug/images/jdk/bin
"


read -p "Press enter to continue"
clear

echo "
During this presentation I've already set up everything. If you want to replicate this
you need to take a look at this script to set up the paths correctly.
"

JAVA_PATH=../../jdk14/build/linux-x86_64-server-fastdebug/images/jdk/bin

read -p "Press enter to continue"
clear

echo "
Finally, let's take a look at the output

Using the version we've generated, compile the code and run it 

To output all the information we need you will need the following flags:

-XX:+UnlockDiagnosticVMOptions      : Enables some extra flags of the JVM
-XX:+PrintAssembly                  : Prints disassembly (also needs a disassembly library available at runtime e.g. hsdis)
-XX:CompileThreshold=1              : Compiles after only 1 call
-XX:CompileOnly=ArrayList::forEach  : Only compiles our method, to make things a little easier to read
-XX:PrintAssemblyOptions=intel      : Intel assembly mode because it is easier to read (IMO)


"

read -p "Press enter to continue"
clear

$JAVA_PATH/javac ArrayList.java 
$JAVA_PATH/java -XX:+UnlockDiagnosticVMOptions -XX:+PrintAssembly -XX:CompileThreshold=1 -XX:CompileOnly=ArrayList::forEach -XX:PrintAssemblyOptions=intel ArrayList > output.asm && pkill java
nvim output.asm

echo "
You can see many deoptimization traps, especially for null checks. This has considerable 
impact on performance. Every time a trap is triggered code goes back to being interpreted.

This is why forEach implementation is actually something like

"

bat ArrayListActual.java
read -p "Press enter to continue"
