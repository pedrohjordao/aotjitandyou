#!/bin/bash -e

clear 

JAVA_PATH=/opt/jdk14/bin

$JAVA_PATH/java -version

read -p "Press enter to continue"
clear

bat TieredCompilation.java


read -p "Press enter to continue"

$JAVA_PATH/javac TieredCompilation.java && $JAVA_PATH/java TieredCompilation | nvim -R

clear

echo "
The following output will interleave the compilation information
and the timings. It ain't pretty, but bear with me.

For this example we are using JDK14, so your output might look
slightly different, here's a general reference:

https://gist.github.com/chrisvest/2932907

First column: Timestamp
Second column: Compilation ID
    This one is tricky, there are 2 compilation IDs going on at
    the same time. 
    Compilation with the % attribute are OSR (On Stack Replacement)
    compilations, we will talk more about these later.
    So compilation IDs 5 and 5% are different compilations.
Third column: Method Attributes
    %: OSR 
    s: Synchronized
    !: Method has exception handler
    b: Blocking Compilation
    n: Native Wrapper
Forth Column: Tiered Compilation Level
Fifth Column: Method Name
Sixth Column: Method Size and Stack Replacement (if applicable)
Seventh Column: 
    made not entrant: Optimization or Deoptimization happened 
    made zombie: no activations after deoptimization happen 
                 (we will not see this one today)

Execution Flags: 
    -XX:+PrintCompilation       Show compilation information
    -XX:-BackgroundCompilation  Does not run Compilation on brackground
                                to make it easier to to see the output
"

read -p "Press enter to continue"

$JAVA_PATH/java -XX:+PrintCompilation -XX:-BackgroundCompilation TieredCompilation | nvim -R
